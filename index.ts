import { prisma } from './generated/prisma-client'

// A `main` function so that we can use async/await
async function main() {  
  const newUser = await prisma.createUser({ name: 'Alexandra' })
  console.log(`Created new user: ${newUser.name} (ID: ${newUser.id})`)

  const allUsers = await prisma.users()
  console.log(allUsers)
}

main().catch(e => console.error(e))